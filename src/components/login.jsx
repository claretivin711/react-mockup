import React, { Component } from "react";
import "./login.css";

class Login extends Component {
  constructor(){
      super()
      this.name = React.createRef()
      this.email = React.createRef()
      this.dob = React.createRef()
      this.phoneNumber = React.createRef()
      this.state = {
        emailError : '',
        phoneNumberError : ''
      }
      this.formValidation = this.formValidation.bind(this)
  }
  formValidation(){
    let validations = {}
    if(this.phoneNumber.current.value.length < 10){
      validations['phoneNumberError'] = '*Phonenumber must have 10 numbers'
    }
    if((/\S+@\S+\.\S+/).test(this.email.current.value) == false){
      validations['emailError'] = '*Please enter a valid email'
    }
    if(Object.keys(validations).length > 0) this.setState(validations)
    else window.location.href = '/Home?Name='+ this.name.current.value + 
            '&Email=' + this.email.current.value +
            '&Dob=' + this.dob.current.value +
            '&PhoneNumber=' + this.phoneNumber.current.value 
  }

  render() {
    return (
        <div className = "form">
          <fieldset>
            <img src="images/man-office1.jpg" />
          </fieldset>
          <fieldset>
            <div>
              <label>Name</label><br></br>
              <input ref = { this.name } type="text" placeholder="Name"></input>
              <br></br>
              <label>Email</label><br></br>
              <input ref = { this.email } type="email" placeholder="Email"></input>
              <br></br>
              <p>{ this.state.emailError }</p>
              <label>Date</label><br></br>
              <input ref = { this.dob } type="date"></input>
              <br></br>
              <label>Phone Number</label><br></br>
              <input ref = { this.phoneNumber } type="text" placeholder="Phone Number"></input>
              <br></br>
              <p>{ this.state.phoneNumberError }</p>
              <button onClick = { this.formValidation }>SUBMIT</button>
            </div>
          </fieldset>
        </div>
    );
  }
}

export default Login;
