import React, { Component } from "react";
import "./home.css";

class Home extends Component {
  render() {
    const url = new URL(window.location);
    return (
      <div className="details">
        <label>Name</label> 
        <h5>{url.searchParams.get("Name")}</h5>
        <br></br>
        <label>Email</label>
        <h5>{url.searchParams.get("Email")}</h5>
        <br></br>
        <label>Date</label>
        <h5>{url.searchParams.get("Dob")}</h5>
        <br></br>
        <label>Phone Number</label>
        <h5>{url.searchParams.get("PhoneNumber")}</h5>
        <br></br>
      </div>
    );
  }
}

export default Home;
